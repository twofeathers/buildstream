#
# Tox global configuration
#
[tox]
envlist = py35,py36,py37
skip_missing_interpreters = true

#
# Defaults for all environments
#
# Anything specified here is iherited by the sections
#
[testenv]
commands =
    pytest --basetemp {envtmpdir} {posargs}
    - mkdir -p .coverage-reports
    - mv {envtmpdir}/.coverage {toxinidir}/.coverage-reports/.coverage.{env:COVERAGE_PREFIX:}{envname}
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt
passenv =
    BST_FORCE_BACKEND
    GI_TYPELIB_PATH
    INTEGRATION_CACHE

#
# These keys are not inherited by any other sections
#
setenv =
    py{35,36,37}: COVERAGE_FILE = {envtmpdir}/.coverage
whitelist_externals =
    py{35,36,37}:
        mv
        mkdir

#
# Coverage reporting
#
[testenv:coverage]
commands =
    - coverage combine --rcfile={toxinidir}/.coveragerc {toxinidir}/.coverage-reports/
    coverage report --rcfile={toxinidir}/.coveragerc -m
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
setenv =
    COVERAGE_FILE = {toxinidir}/.coverage-reports/.coverage

#
# Running linters
#
[testenv:lint]
commands =
    pycodestyle
    pylint buildstream
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt

#
# Building documentation
#
[testenv:docs]
commands =
    make -C doc
# sphinx_rtd_theme < 0.4.2 breaks search functionality for Sphinx >= 1.8
deps =
    sphinx
    sphinx-click
    sphinx_rtd_theme >= 0.4.2
    pytest
    -rrequirements/requirements.txt
    -rrequirements/plugin-requirements.txt
passenv =
    BST_FORCE_SESSION_REBUILD
    BST_SOURCE_CACHE
    HOME
    LANG
    LC_ALL
whitelist_externals =
    make

#
# (re-)Generating man pages
#
[testenv:man]
commands =
    python3 setup.py --command-packages=click_man.commands man_pages
deps =
    click-man >= 0.3.0
    -rrequirements/requirements.txt

#
# Usefull for running arbitrary scripts in a BuildStream virtual env
#
[testenv:venv]
commands = {posargs}
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt
whitelist_externals = *
